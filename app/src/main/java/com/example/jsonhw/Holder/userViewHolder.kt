package com.example.jsonhw.Holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.jsonhw.Listener.OnitemClickedListener
import com.example.jsonhw.Model.userModel
import kotlinx.android.synthetic.main.user_adapter.view.*

class userViewHolder(var view : View) : RecyclerView.ViewHolder(view) {

    fun bindViewHolder(position: Int, listener: OnitemClickedListener, dataList : ArrayList<userModel>) {
        val model = dataList[position]
        val geo = model.address!!.geo.toString()

        view.rootView.setOnClickListener {
            listener.onCliked(position)
        }
        view.textName.text = model.name
        view.textEmail.text = model.email
        view.textTel.text = model.phone
    }
}