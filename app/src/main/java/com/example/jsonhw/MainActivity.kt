package com.example.jsonhw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.jsonhw.Listener.OnNavigateYoUserDetailListener
import com.example.jsonhw.Model.userModel
import com.example.jsonhw.fragment.UserDetailFragment
import com.example.jsonhw.fragment.UserProfileFragment

class MainActivity : AppCompatActivity(), OnNavigateYoUserDetailListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            Log.d("MainActivity","replace")
            replaceUserProfileFragment()
        }
    }

    private fun replaceUserProfileFragment() {
        val fragment = UserProfileFragment.newInstance()
        fragment.listener = this@MainActivity
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }

    override fun navigateToUserDetailFragment(model : userModel) {
        replaceUserDetailFragment(model)
    }

    private fun replaceUserDetailFragment(model: userModel) {
        val fragment = UserDetailFragment.newInstance(model)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack("User_detail")
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

}
