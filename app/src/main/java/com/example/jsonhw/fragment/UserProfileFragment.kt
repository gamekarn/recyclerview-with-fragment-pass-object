package com.example.jsonhw.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jsonhw.Adapter.userAdapter
import com.example.jsonhw.Listener.OnNavigateYoUserDetailListener
import com.example.jsonhw.Listener.OnitemClickedListener
import com.example.jsonhw.Model.userModel
import com.example.jsonhw.R
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_user_profile.*
import okhttp3.*
import java.io.IOException

class UserProfileFragment : Fragment(), OnitemClickedListener{

    lateinit var listener: OnNavigateYoUserDetailListener
    private lateinit var dataList: ArrayList<userModel>
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapter: userAdapter


    companion object {
        fun newInstance(): UserProfileFragment {
            return UserProfileFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        when(context) {
            is OnNavigateYoUserDetailListener -> listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            dataList = arrayListOf()

        } else {
            dataList = savedInstanceState.getParcelableArrayList("Userlist")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLayoutManager()
        setupRecyclerView()
    }

    private fun setLayoutManager() {
        layoutManager = LinearLayoutManager(activity)
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (dataList.isEmpty()) {
            getUser()
            Log.d("UserProfile", "getUser")
        } else {
            setAdapter()
        }
    }

    private fun getUser() {
        val endpoint = "https://jsonplaceholder.typicode.com/users"
        Log.d("MainActivity","get User")
        val client = OkHttpClient()

        val request = Request.Builder()
            .url(endpoint)
            .build()

        val call = client.newCall(request).enqueue(object : Callback {

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body?.let {
                        val data = it.string()
                        mapDataToJson(data)
                        activity?.runOnUiThread {
                            setAdapter()
                        }
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {

            }

        })
    }

    private fun mapDataToJson(data : String) {
        val gson = Gson()
        val collectionType = object : TypeToken<List<userModel>>() {}.type
        dataList = gson.fromJson<String>(data, collectionType) as ArrayList<userModel>
        activity?.runOnUiThread {
            setAdapter()
        }
    }



    private fun setAdapter() {
        adapter = userAdapter()
        adapter.listener = this@UserProfileFragment
        adapter.dataList = dataList
        recyclerView.adapter = adapter
    }

    override fun onCliked(position: Int) {
        println("this position is $position")

        var data = dataList!![position]
        listener.navigateToUserDetailFragment(data)
//
//        Log.d(".MainActivity", "this is $data")
//
//        val intent = Intent(this , user_profile :: class.java)
//        intent.putExtra("userInfo", data)
//        startActivity(intent)
    }

    override fun OnCheckedChangeListener(i: Int) {

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("Userlist" , dataList)
        Log.d("MainActivity", "OnSaveInstanceState")
    }

}