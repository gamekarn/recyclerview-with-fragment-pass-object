package com.example.jsonhw.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jsonhw.Listener.OnNavigateYoUserDetailListener
import com.example.jsonhw.Model.userModel
import com.example.jsonhw.R
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.fragment_user_detail.*
import kotlinx.android.synthetic.main.fragment_user_detail.edtAdress
import kotlinx.android.synthetic.main.fragment_user_detail.edtCompany
import kotlinx.android.synthetic.main.fragment_user_detail.edtEmail
import kotlinx.android.synthetic.main.fragment_user_detail.edtId
import kotlinx.android.synthetic.main.fragment_user_detail.edtName
import kotlinx.android.synthetic.main.fragment_user_detail.edtPhone
import kotlinx.android.synthetic.main.fragment_user_detail.edtUsername
import kotlinx.android.synthetic.main.fragment_user_detail.edtWebsite

class UserDetailFragment: Fragment() {
    lateinit var data : userModel


    companion object {
        fun newInstance(data : userModel): UserDetailFragment {
            val fragment = UserDetailFragment()

            val bundle = Bundle()
            bundle.putParcelable("data", data)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null) {
            arguments?.let {
                data = it.getParcelable("data")!!
            }
        } else {
            data = savedInstanceState.getParcelable("data")!!
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var street = data.address!!.street.toString()
        var suit = data.address!!.suit.toString()
        var city = data.address!!.city.toString()
        var zipcode = data.address!!.zipcode.toString()
        var geolat = data.address!!.geo!!.lat.toString()
        var geolng = data.address!!.geo!!.lng.toString()

        var companyName = data.company!!.name.toString()
        var catchPhrase = data.company!!.catchPhrase.toString()
        var bs = data.company!!.bs.toString()

        edtId.text = data.id.toString()
        edtId.text = data.id.toString()
        edtName.text = data.name.toString()
        edtUsername.text = data.username.toString()
        edtEmail.text = data.email.toString()
        edtAdress.text = "Street: $street, Suit: $suit, City: $city, Zipcode: $zipcode, Geo: $geolat $geolng "
        edtPhone.text = data.phone.toString()
        edtWebsite.text = data.website.toString()
        edtCompany.text = "Company Name: $companyName \nCatch Phrase: $catchPhrase \nBS: $bs"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_detail, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("data",data)
    }


}