package com.example.jsonhw.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.example.jsonhw.Model.userModel
import com.example.jsonhw.R
import com.example.jsonhw.Listener.OnitemClickedListener
import com.example.jsonhw.Holder.userViewHolder as userViewHolder1

class userAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener : OnitemClickedListener
    var dataList: ArrayList<userModel> = arrayListOf()

    override fun getItemCount(): Int {
        return dataList.size;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(
                R.layout.user_adapter,
                parent,false)
        return userViewHolder1(view)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is com.example.jsonhw.Holder.userViewHolder -> {
                holder.bindViewHolder(position,listener,dataList)
            }
        }
    }
}