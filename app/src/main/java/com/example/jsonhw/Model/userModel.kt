package com.example.jsonhw.Model

import android.os.Parcel
import android.os.Parcelable


data class userModel(
    var id: Int,
    var name: String?,
    var username: String?,
    var email: String?,
    var address: userAdress?,
    var phone: String?,
    var website: String?,
    var company: userCompany?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(userAdress :: class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable<userCompany?>(userCompany :: class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(username)
        parcel.writeString(email)
        parcel.writeParcelable(address, flags)
        parcel.writeString(phone)
        parcel.writeString(website)
        parcel.writeParcelable(company, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<userModel> {
        override fun createFromParcel(parcel: Parcel): userModel {
            return userModel(parcel)
        }

        override fun newArray(size: Int): Array<userModel?> {
            return arrayOfNulls(size)
        }
    }
}

data class userAdress(
    var street: String?,
    var suit: String?,
    var city: String?,
    var zipcode: String?,
    var geo: userGeo?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable<userGeo>(userGeo :: class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(street)
        parcel.writeString(suit)
        parcel.writeString(city)
        parcel.writeString(zipcode)
        parcel.writeParcelable(geo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<userAdress> {
        override fun createFromParcel(parcel: Parcel): userAdress {
            return userAdress(parcel)
        }

        override fun newArray(size: Int): Array<userAdress?> {
            return arrayOfNulls(size)
        }
    }
}

data class userGeo(
    var lat: Double,
    var lng: Double
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lng)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<userGeo> {
        override fun createFromParcel(parcel: Parcel): userGeo {
            return userGeo(parcel)
        }

        override fun newArray(size: Int): Array<userGeo?> {
            return arrayOfNulls(size)
        }
    }
}

data class userCompany(
    var name: String?,
    var catchPhrase: String?,
    var bs: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(catchPhrase)
        parcel.writeString(bs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<userCompany> {
        override fun createFromParcel(parcel: Parcel): userCompany {
            return userCompany(parcel)
        }

        override fun newArray(size: Int): Array<userCompany?> {
            return arrayOfNulls(size)
        }
    }
}

