package com.example.jsonhw.Listener

import com.example.jsonhw.Model.userModel

interface OnNavigateYoUserDetailListener {
    fun navigateToUserDetailFragment(model : userModel)
}