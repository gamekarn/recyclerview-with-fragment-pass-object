package com.example.user_profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jsonhw.Model.userModel
import com.example.jsonhw.R
import kotlinx.android.synthetic.main.activity_user_profile.*

class user_profile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        val acc = intent.getParcelableExtra<userModel>("userInfo")
        var street = acc.address!!.street.toString()
        var suit = acc.address!!.suit.toString()
        var city = acc.address!!.city.toString()
        var zipcode = acc.address!!.zipcode.toString()
        var geolat = acc.address!!.geo!!.lat.toString()
        var geolng = acc.address!!.geo!!.lng.toString()

        var companyName = acc.company!!.name.toString()
        var catchPhrase = acc.company!!.catchPhrase.toString()
        var bs = acc.company!!.bs.toString()

        edtId.text = acc.id.toString()
        edtName.text = acc.name.toString()
        edtUsername.text = acc.username.toString()
        edtEmail.text = acc.email.toString()
        edtAdress.text = "Street: $street, Suit: $suit, City: $city, Zipcode: $zipcode, Geo: $geolat $geolng "
        edtPhone.text = acc.phone.toString()
        edtWebsite.text = acc.website.toString()
        edtCompany.text = "Company Name: $companyName \nCatch Phrase: $catchPhrase \nBS: $bs"
    }
}
